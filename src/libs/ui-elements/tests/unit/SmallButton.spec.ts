import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import SmallButton from '../../src/small-button/SmallButton.vue';

describe('BigButton.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'Me is Small';
    const wrapper = shallowMount(SmallButton, {
      props: { label },
    });
    expect(wrapper.text()).to.include(label);
  });
});
