import BigButtonVue from './src/big-button/BigButton.vue';
import SmallButtonVue from './src/small-button/SmallButton.vue';

const BigButton = BigButtonVue;
const SmallButton = SmallButtonVue;

export { BigButton };
export { SmallButton };
