import { expect } from 'chai';
import { getFormattedDateTime } from '..';

describe('date-helper.getFormattedDateTime', () => {
  it('should format date correctly', () => {
    expect(getFormattedDateTime('2021-12-24', '20:14:12')).to.equal('24.12.2021 20:14');
  });
});
