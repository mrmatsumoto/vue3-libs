# Helpers Lib

This lib contains simple helpers, that perform simple data wrangling.

## Usage

Add the following code to paths in your tsconfig file:

```
"@libs/helpers": [
    "libs/helpers"
]
```

### Correct

Import only the components and classes, that are provided by index.ts.

```
import { getFormattedDateTime } from '@/libs/helpers';
```

### Not Correct

Do not use relative paths, or direct access to files inside the lib.

```
import { getFormattedDateTime } from '@/libs/helpers/src/date-helper';
```
