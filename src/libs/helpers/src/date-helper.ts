export const today = (): string => new Date().toISOString().substring(0, 10);

export const now = (): string => new Date().toLocaleTimeString('DE-de').substring(0, 5);

export const getDateString = (fullDate: string): string => (fullDate ? fullDate.substring(0, 10) : '');

export const getTimeString = (fullDate: string): string => {
  if (!fullDate) return '';
  const date = new Date(fullDate);
  const getHours = date.getHours();
  const getMinutes = date.getMinutes();
  return `${(`0${getHours}`).slice(-2)}:${(`0${getMinutes}`).slice(-2)}`;
};

export const getFormattedDateTime = (date: string, time: string): string => {
  const dateString = date || new Date().toISOString().substring(0, 10);
  const formattedDate = `${dateString.substring(8, 10)}.${dateString.substring(5, 7)}.${dateString.substring(0, 4)}`;
  const formattedTime = time ? time.substring(0, 5) : new Date().toLocaleTimeString('DE-de', { hour: '2-digit', minute: '2-digit' });

  return `${formattedDate} ${formattedTime}`;
};
