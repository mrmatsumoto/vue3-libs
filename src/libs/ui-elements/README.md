# UI-Elements Lib

This lib only contains components for UI. No business logic, data access or services should be placed inside this lib.

## Usage

Add the following code to paths in your tsconfig file:

```
"@libs/ui-elements": [
    "libs/ui-elements"
]
```

### Correct

Import only the components and classes, that are provided by index.ts.

```
import { BigButton } from '@/libs/ui-elements';
```

### Not Correct

Do not use relative paths, or direct access to files inside the lib.

```
import { BigButton } from '@/libs/ui-elements/src/big-button/BigButton';
import { BigButton } from '../../libs/ui-elements/src/big-button/BigButton';
```
