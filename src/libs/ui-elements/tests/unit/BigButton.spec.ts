import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import BigButton from '../../src/big-button/BigButton.vue';

describe('BigButton.vue', () => {
  it('renders props.label when passed', () => {
    const label = 'Me is Big';
    const wrapper = shallowMount(BigButton, {
      props: { label },
    });
    expect(wrapper.text()).to.include(label);
  });
});
