import { defineComponent } from 'vue';

export default defineComponent({
  name: 'BigButton',
  props: {
    label: {
      default: 'Click Me',
      type: String,
    },
  },
});
